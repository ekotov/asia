<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "asia_data".
 *
 * @property int $id
 * @property string $fio
 * @property string $email
 * @property int $color
 */
class AsiaData extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'asia_data';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['color'], 'integer'],
            [['fio', 'email'], 'string', 'max' => 255],
            [['email'], 'email'],
            [['email'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fio' => 'Fio',
            'email' => 'Email',
            'color' => 'Color',
        ];
    }
}
