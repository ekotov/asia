<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%}}`.
 */
class m200115_105029_create_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('asia_data', [
            'id' => $this->primaryKey(),
            'fio' => $this->string()->notNull()->defaultValue(''),
            'email' => $this->string()->notNull()->unique()->defaultValue(''),
            'color' => $this->integer()->notNull()->defaultValue(0),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('asia_data');
    }
}
