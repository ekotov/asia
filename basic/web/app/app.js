var app = new Vue({
    'vue-resource': 'vue-resource',
    el: '#app',
    data: {
        items: [{color: 6,
            email: "zz@zz.zz",
            fio: "zzz",
            id: 1,}],
        form: {
            color: 5,
            email: "",
            fio: "",
        },
        errors: {
            status: "",
            email: {value: false, text: 'Неверно введен email'},
            fio: {value: false, text: 'Только латинские буквы'}
        },
        values: [
            {value: 1, color: '', label: "зелёный"},
            {value: 2, color: '', label: "зелёный"},
            {value: 3, color: '', label: "зелёный"},
            {value: 4, color: '', label: "жёлтый"},
            {value: 5, color: '', label: "жёлтый"},
            {value: 6, color: '', label: "жёлтый"},
            {value: 7, color: '', label: "красныый"},
            {value: 8, color: '', label: "красныый"},
            {value: 9, color: '', label: "чёрный"},
            {value: 10, color: '', label: "чёрный"},
        ]
    },
    created: function() {
        this.getList()
    },
    methods: {
        validateFio: function() {
            let ret = /^[a-zA-Z]+$/.test(this.form.fio);
            this.errors.fio.value = !ret;
            return ret;
        },
        validateEmail: function() {
            const regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;;
            let ret = regex.test(this.form.email);
            this.errors.email.value = !ret;
            return ret;
        },
        formValid: function() {
          return this.validateEmail() && this.validateFio()
        },
        saveData: function () {
            if (this.formValid()) {
                this.$http.post('/save-data', this.form)
                    .then(response => {
                        this.errors.status = "";
                        this.getList();
                    })
                    .catch(e => {
                        this.errors.status = e.statusText
                    })
            } else {

            }
        },
        resetError: function () {
            this.errors.email.value = false;
            this.errors.fio.value = false;
        },
        getList: function () {
            this.$http.get('/save-data', this.form)
                .then(response => {
                    this.items = response.data;
                })
                .catch(e => console.log(e))
        }
    }
});