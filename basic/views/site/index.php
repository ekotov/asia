<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">
    <div class="body-content">
        <div id="app">
        <div class="row">
            <div class="col-lg-12">

                    <form @submit.prevent="saveData">
                        <div class="radio">
                            <label v-for="item in values" :for="item.value" class="radio-inline">
                                <input type="radio" v-model="form.color" :value="item.value" :id="item.value">
                                {{item.label}}</label>
                        </div>
                        <div class="form-group" :class="{'has-error': errors.email.value}">
                            <label for="email">Email address</label>
                            <input v-on:input="validateEmail" type="email" v-model="form.email" class="form-control"
                                   id="email" placeholder="Email">
                        </div>
                        <div class="form-group" :class="{'has-error': errors.fio.value}">
                            <label for="fio">ФИО</label>
                            <input v-on:input="validateFio" type="text" v-model="form.fio" class="form-control"
                                   id="fio" placeholder="ФИО">
                        </div>
                        <div class="error" v-if="errors.status !== ''">{{errors.status}}</div>
                        <button type="submit" class="btn btn-default" :disabled="!formValid()">Submit</button>
                    </form>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <table class="table table-striped">
                    <tr>
                        <th>color</th>
                        <th>email</th>
                        <th>fio</th>
                    </tr>
                    <tr v-for="data in items">
                        <td>{{data.color}}</td>
                        <td>{{data.email}}</td>
                        <td>{{data.fio}}</td>
                    </tr>
                </table>
            </div>
        </div>
        </div>
    </div>
</div>
